#!/bin/bash
set -ex
BOOTSTRAP=1 vagrant up && BOOTSTRAP=1 vagrant provision
vagrant halt
vagrant up && vagrant provision
