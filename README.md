# Bootstrap debian for playing with ansible on vagrant virtualbox boxes
To get started:
```
pushd files && debian_bootstrap.sh && vagrant up
```` 

The vagrantfile is provided for testing e2e on a debian-buster VM. Run the test using `./run_test.sh`.

You must also load the vboxdrv module. This is not signed so you will either have to disable secure boot or sign it manually.

```
# modprobe vboxdrv
modprobe: ERROR: could not insert 'vboxdrv': Required key not available
```

```
# /sbin/vboxconfig
```
