#!/bin/bash
set -ex
sudo apt update
sudo apt install -y \
  python3 \
  python3-pip \
  git
sudo python3 -m pip install ansible
ansible-galaxy install --role-file="roles.yml"
ansible-playbook bootstrap.yml -e "user=$(whoami)"
